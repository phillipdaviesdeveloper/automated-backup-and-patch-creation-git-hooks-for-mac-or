Post Commit backup and release hook
-----------------------------------

Hooks for commit and merge actions, there are two different version, mac and windows.

Files will need to be customised per project to assign the correct storage locations.

Windows hook files:
	win.post-commit
	win.post-merge


Mac files:
	mac.post-commit
	mac.post-merge
	
	
(note: Mac files may also work on linux although currently untested)

